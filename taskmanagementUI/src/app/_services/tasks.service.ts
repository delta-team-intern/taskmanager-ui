import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GetTask } from '../_models/get-task.model';
import { EditTask } from '../_models/edit-task.model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { AddTask } from '../_models/add-task.model';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  getAllTasksUrl = environment.baseUrl + '/api/taskunit/getalltaskunits';
  getTaskByIdUrl = environment.baseUrl + '/api/taskunit/gettaskunitbyid';
  updateTaskUrl = environment.baseUrl + '/api/taskunit/updatetaskunit';
  deleteTaskUrl = environment.baseUrl + '/api/taskunit/delete';
  createTaskUrl = environment.baseUrl + '/api/taskunit/savetaskunit';

  constructor(private httpClient: HttpClient) {}

  getTasks(): Observable<GetTask[]> {
    return this.httpClient.get<GetTask[]>(this.getAllTasksUrl);
  }

  deleteTaskUnit(id: number) {
    this.httpClient.delete(this.deleteTaskUrl + '/' + id).subscribe((data) => {
      alert('Task ' + id + ': Successfully removed!');
    });
  }

  getTaskById(id: number): Observable<GetTask> {
    return this.httpClient.get<GetTask>(this.getTaskByIdUrl + '/' + id);
  }

  updateTask(task: EditTask): void {
    this.httpClient.put(this.updateTaskUrl, task).subscribe((data) => {
      alert('Task successfully updated!');
    });
  }

  createNewTask(task = <AddTask>{}): void {
    this.httpClient.post<any>(this.createTaskUrl, task).subscribe((data) => {
      alert('Task successfully created!');
    });
  }
}
