export interface EditTask {
  name: string;
  description: string;
  startDate: string;
  endDate: string;
  status: string;
  priority: string;
  assignedUsers: Array<string>;
}
