import { Collection } from 'ngx-pagination/dist/paginate.pipe';
import { User } from './user.model';

export interface GetTask {
  taskUnitId: number;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
  status: string;
  priority: string;
  assignedUsers: [];
}
