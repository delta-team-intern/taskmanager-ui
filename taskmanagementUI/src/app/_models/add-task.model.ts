import { Collection } from 'ngx-pagination/dist/paginate.pipe';
import { User } from '../_models/user.model';

export interface AddTask {
  taskUnitId: number;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
  status: string;
  priority: string;
  assignedUsers: Array<string>;
}
