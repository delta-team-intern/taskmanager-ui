import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { GetTask } from '../_models/get-task.model';
import { TasksService } from '../_services/tasks.service';
import { UserService } from '../_services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../_models/user.model';
import { EditTask } from '../_models/edit-task.model';
import { RouterModule, Routes } from '@angular/router';
import { $ } from 'protractor';
import { Observable } from 'rxjs';

/**
 * @title Basic Inputs
 */
@Component({
  selector: 'edit-task',
  styleUrls: ['edit-task.component.css'],
  templateUrl: 'edit-task.component.html',
})
export class EditTaskComponent {
  task: GetTask;
  taskId: number;
  assignedUsers: User[];
  tempData: [];
  editTask: EditTask;
  tempArray: Array<string>;

  constructor(
    private taskService: TasksService,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.taskId = params['id'];
    });

    this.getTaskById(this.taskId).subscribe((data: any) => {
      this.task = data;
      this.editTask = this.task;
      this.tempArray = [];
      this.task.assignedUsers.forEach((user: any) => {
        this.tempArray.push(user.assignedUserId);
      });
      this.editTask.assignedUsers = this.tempArray;
    });

    this.userService.getAllUsers().subscribe((data) => {
      this.assignedUsers = data;
    });
  }

  getTaskById(id: number): Observable<GetTask> {
    return this.taskService.getTaskById(id);
  }

  updateTask(): void {
    this.taskService.updateTask(this.task);
    this.router.navigate(['/dashboard']);
  }

  changeTaskAssignedUsers(e): void {
    this.editTask.assignedUsers = e;
  }

  changeTaskStatus(e): void {
    this.editTask.status = e;
  }

  changeTaskPriority(e): void {
    this.editTask.priority = e;
  }
}
