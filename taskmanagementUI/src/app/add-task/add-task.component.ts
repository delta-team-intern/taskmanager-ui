import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { TasksService } from '../_services/tasks.service';
import { UserService } from '../_services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../_models/user.model';
import { AddTask } from '../_models/add-task.model';
import { RouterModule, Routes } from '@angular/router';
import { $ } from 'protractor';

/**
 * @title Basic Inputs
 */
@Component({
  selector: 'add-task',
  styleUrls: ['add-task.component.css'],
  templateUrl: 'add-task.component.html',
})
export class AddTaskComponent {
  assignedUsers: User[];
  addTask = <AddTask>{};

  constructor(
    private taskService: TasksService,
    private route: ActivatedRoute,
    private usersService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.usersService.getAllUsers().subscribe((data) => {
      this.assignedUsers = data;
    });
  }

  crateNewTask(): void {
    this.taskService.createNewTask(this.addTask);
    this.router.navigate(['/dashboard']);
  }

  changeTaskAssignedUsers(e): void {
    this.addTask.assignedUsers = e;
  }

  changeTaskStatus(e): void {
    this.addTask.status = e;
  }

  changeTaskPriority(e): void {
    this.addTask.priority = e;
  }
}
